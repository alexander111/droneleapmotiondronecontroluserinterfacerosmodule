//////////////////////////////////////////////////////
//  droneLeapMotionDroneControlUserInterfaceROSModuleNode.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 23, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>



// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"


//Drone Module
//#include "droneModuleROS.h"

//trajectory planner
#include "droneLeapMotionDroneControlUserInterfaceROSModule.h"

//Communication Definition
//MODULE_NAME_TRAJECTORY_PLANNER
#include "communication_definition.h"






using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "DRONE_LEAP_MOTION_INTERFACE"); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    cout<<"[ROSNODE] Starting Drone Leap Motion Interface"<<endl;

    //Trajectory planner
    DroneLeapMotionInterfaceROSModule MyDroneLeapMotionInterfaceROSModule;
    MyDroneLeapMotionInterfaceROSModule.open(n,"DRONE_LEAP_MOTION_INTERFACE");

    //Loop
    while(ros::ok())
    {
        //Read ros messages
        ros::spin();
    }

    return 1;
}

